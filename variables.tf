variable "cluster_prefix_name" {}

variable "region" {
  default = "us-east-1"
}

variable "aws_elb_hostname" {
  default = ""
}

variable "route53_record_name" {
  default = "test.adm-dokuwiki.tk"
}

variable "aws_zone_id" {
  default = ""
}
